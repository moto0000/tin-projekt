import Vapor
import Paginator
import Authentication

final class ChildController: CreatableController, UpdateableController {
    typealias Model = Child

    var path: String {
        return "child"
    }

    func renderList(_ req: Request) throws -> Future<View> {
        let paginator: Future<OffsetPaginator<Child>> = try Child.query(on: req).paginate(for: req)
        return paginator.flatMap(to: View.self) { paginator in
            return try req.leaf().render(
                "Child/index",
                ["children": paginator.data ?? []],
                userInfo: paginator.userInfo()
            )
        }
    }

    func delete(_ req: Request) throws -> Future<Response> {
        return try req.parameters.next(Child.self).flatMap { child in
            let redirect = req
                .redirect(to: "/" + self.path)
                .flash(.success, "\"\(child.name)\" został usunięty")
            return child.delete(on: req).transform(to: redirect)
        }
    }
}

extension ChildController: RouteCollection {

    func boot(router: Router) throws {
        let protected = router.grouped(RedirectMiddleware<Admin>(path: "/user/login"))

        protected.get(path, use: renderList)
        protected.get(path, Child.parameter, "delete", use: delete)
        try bootCreate(router: protected)
        try bootUpdate(router: protected)
    }
}

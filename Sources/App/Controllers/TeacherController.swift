import Vapor
import Paginator
import Authentication

final class TeacherController: CreatableController, UpdateableController {
    typealias Model = Teacher

    var path: String {
        return "teacher"
    }

    func renderList(_ req: Request) throws -> Future<View> {
        let paginator: Future<OffsetPaginator<Teacher>> = try Teacher.query(on: req).paginate(for: req)
        return paginator.flatMap(to: View.self) { paginator in
            return try req.leaf().render(
                "Teacher/index",
                ["teachers": paginator.data ?? []],
                userInfo: paginator.userInfo()
            )
        }
    }

    func delete(_ req: Request) throws -> Future<Response> {
        return try req.parameters.next(Teacher.self).flatMap { teacher in
            let redirect = req
                .redirect(to: "/" + self.path)
                .flash(.success, "\"\(teacher.name)\" został usunięty")
            return teacher.delete(on: req).transform(to: redirect)
        }
    }

    private func groups(on req: Request) -> Future<[Group]> {
        return Group.query(on: req).filter(\.endDate, .greaterThanOrEqual, .current).all()
    }
}

extension TeacherController: RouteCollection {

    func boot(router: Router) throws {
        router.get(path, use: renderList)

        let protected = router.grouped(RedirectMiddleware<Admin>(path: "/user/login"))
        protected.get(path, Teacher.parameter, "delete", use: delete)
        try bootCreate(router: protected)
        try bootUpdate(router: protected)
    }
}

struct UpdateTeacherViewContext: Content {
    let model: Teacher
    let groups: [Group]
}

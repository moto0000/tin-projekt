import Leaf
import Vapor
import Fluent
import PostgreSQL
import Paginator
import Authentication

final class GroupController: CreatableController, UpdateableController {
    typealias Model = Group

    var path: String {
        return "group"
    }

    func renderList(_ req: Request) throws -> Future<View> {
        let paginator: Future<OffsetPaginator<Group>> = try Group.query(on: req).paginate(for: req)
        return paginator.flatMap(to: View.self) { paginator in
            return try req.leaf().render(
                "Group/index",
                ["groups": paginator.data ?? []],
                userInfo: try paginator.userInfo()
            )
        }
    }

    func create(_ req: Request) throws -> Future<Response> {
        return try req.content.decode(Model.Submission.self)
            .createValid(on: req)
            .save(on: req)
            .transform(to: redirectSuccess(on: req))
            .catchFlatMap { error in
                return Teacher.query(on: req).all().flatMap(to: Response.self) { teachers in
                    return try req.leaf().render(self.createView, ["teachers": teachers]).flatMap { view in
                        try view.encode(for: req)
                    }
                }
        }
    }

    private func redirectSuccess(on req: Request) -> Response {
        return req
            .redirect(to: "/\(successRedirectPath)")
            .flash(.success, "OK")
    }

    func renderCreate(_ req: Request) throws -> Future<View> {
        try req.populateFields(Group.self)
        return Teacher.query(on: req).all().flatMap { teachers in
            return try req.leaf().render(
                self.createView,
                ["teachers": teachers]
            )
        }
    }

    func update(_ req: Request) throws -> Future<Response> {
        return try req.parameters.next(Model.self)
            .updateValid(on: req)
            .save(on: req)
            .transform(to: req.redirect(to: "/\(path)").flash(.success, "Update OK!"))
            .catchFlatMap { error in
                return Teacher.query(on: req).all().flatMap(to: Response.self) { teachers in
                    return try req.leaf().render(self.updateView, ["teachers": teachers]).flatMap { view in
                        try view.encode(for: req)
                    }
                }
            }
            // .handleCreateOrUpdateError(on: req, view: updateView)
    }

    func renderUpdate(_ req: Request) throws -> Future<View> {
        return try req.parameters.next(Group.self)
            .populateFields(on: req)
            .flatMap(to: View.self) { group in
                return Teacher.query(on: req).all().flatMap(to: View.self) { teachers in
                    return try req.leaf().render(
                        self.updateView,
                        UpdateGroupViewContext(model: group, teachers: teachers)
                    )
                }
            }
    }

    func delete(_ req: Request) throws -> Future<Response> {
        return try req.parameters.next(Group.self).flatMap { group in
            let redirect = req
                .redirect(to: "/group")
                .flash(.success, "Grupa \"\(group.name)\" została usunięta")
            return group.children.detachAll(on: req).flatMap {
                return group.delete(on: req).transform(to: redirect)
            }
        }
    }

    func renderAddChild(_ req: Request) throws -> Future<View> {
        return try req.parameters.next(Group.self).flatMap { group in
            return Group.query(on: req).filter(\.endDate, .greaterThanOrEqual, .current).all().flatMap { groups in
                return Child.query(on: req).all().flatMap { children in
                    return ChildGroup.query(on: req).all().flatMap { childGroup in
                        let fChildren = children.filter { child in
                            return !childGroup.contains(where: { cg in
                                return cg.childId == child.id!
                                    && groups.contains { $0.id! == cg.groupId }
                            })
                        }

                        return try req.leaf().render(
                            "Group/add-child",
                            AddChildrenViewContext(
                                group: group,
                                children: fChildren
                            )
                        )
                    }
                    
                }
            }
        }
    }

    func addChild(_ req: Request, children: AddChildrenRequest) throws -> Future<Response> {
        return try req.parameters.next(Group.self).flatMap { group in
            let values = try children.children.map { childId in
                return try ChildGroup(childId: childId, groupId: group.requireID())
            }

            return req.transaction(on: .psql) { conn in
                return conn.insert(into: ChildGroup.self).values(values).run()
            }.map(to: Response.self) { _ in
                return req.redirect(to: "/group/\(group.id!)").flash(.success, "OK")
            }
        }
    }

    func removeChild(_ req: Request) throws -> Future<Response> {
        return try req.parameters.next(Group.self).flatMap { group in
            return try req.parameters.next(Child.self).flatMap { child in
                let redirect = try req
                    .redirect(to: "/group/\(group.requireID())")
                    .flash(.success, "\(child.name) został(a) usunięty z grupy")
                return group.children
                    .detach(child, on: req)
                    .transform(to: redirect)
            }
        }
    }

    func renderDetails(_ req: Request) throws -> Future<View> {
        return try req.parameters.next(Group.self).flatMap { group in
            return group.teacher.query(on: req).first().flatMap { teacher in
                return try group.children.query(on: req).all().flatMap { children in
                    return try req.leaf().render(
                        "Group/details",
                        GetGroupResponse(
                            id: group.requireID(),
                            name: group.name,
                            description: group.description,
                            startDate: group.startDate,
                            endDate: group.endDate,
                            maxChildrenCount: group.maxChildsCount,
                            teacher: teacher,
                            children: children
                        )
                    )
                }
            }
        }
    }
}

extension GroupController: RouteCollection {

    func boot(router: Router) throws {
        router.get(path, use: renderList)
        router.get(path, Group.parameter, use: renderDetails)

        let protected = router.grouped(RedirectMiddleware<Admin>(path: "/user/login"))
        protected.get(path, Group.parameter, "delete", use: delete)

        protected.get(path, Group.parameter, "add-child", use: renderAddChild)
        protected.post(AddChildrenRequest.self, at: path, Group.parameter, "add-child", use: addChild)

        protected.get(path, Group.parameter, "remove-child", Child.parameter, use: removeChild)

        try bootCreate(router: protected)
        try bootUpdate(router: protected)
    }
}

struct UpdateGroupViewContext: Content {
    let model: Group
    let teachers: [Teacher]
}

struct AddChildrenViewContext: Content {
    let group: Group
    let children: [Child]
}

struct AddChildrenRequest: Content {
    let children: [Int]
}

struct GetChildResponse: Content {
    let id: Int
    let name: String
    let lastName: String
    let dateOfBirth: Date
    let phoneNumber: String
    let description: String
}

struct GetGroupResponse: Content {
    let id: Int
    let name: String
    let description: String
    let startDate: Date
    let endDate: Date
    let maxChildrenCount: Int
    let teacher: Teacher?
    let children: [Child]
}

extension Date {
    
    static var current: Date {
        return .init()
    }
}

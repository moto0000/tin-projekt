import Leaf
import Vapor
import Crypto

final class UserController: CreatableController {
    typealias Model = Admin

    var path: String {
        return "user"
    }

    var createView: String {
        return "register"
    }

    var successRedirectPath: String {
        return ""
    }

    func renderLogin(_ req: Request) throws -> Future<View> {
        return try req.leaf().render("login")
    }

    func login(_ req: Request, _ login: AdminLoginRequest) throws -> Future<Response> {
        return Admin
            .authenticate(username: login.email, password: login.password, using: BCrypt, on: req)
            .map { user in
                guard let user = user else {
                    return req.redirect(to: "/user/login").flash(.error, "Taki użytkownik nie istnieje")
                }

                try req.authenticateSession(user)
                return req.redirect(to: "/")
            }
    }

    func logout(_ req: Request) throws -> Future<Response> {
        try req.unauthenticateSession(Admin.self)
        return Future.map(on: req) {
            return req.redirect(to: "/user/login")
        }
    }
}

extension UserController: RouteCollection {

    func boot(router: Router) throws {
        router.group(path) { router in
            router.get("login", use: renderLogin)
            router.post(AdminLoginRequest.self, at: "login", use: login)
            router.get("logout", use: logout)
        }

        try bootCreate(router: router)
    }
}

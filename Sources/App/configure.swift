import Leaf
import Vapor
import Flash
import Sugar
import Paginator
import Bootstrap
import Submissions
import Authentication
import FluentPostgreSQL
import TemplateKit

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    let serverConfigure = NIOServerConfig.default(hostname: "localhost", port: 9090)
    services.register(serverConfigure)

    try services.register(FluentPostgreSQLProvider())
    try services.register(LeafProvider())
    try services.register(FlashProvider())
    try services.register(BootstrapProvider())
    try services.register(SubmissionsProvider())
    try services.register(AuthenticationProvider())
    try services.register(CurrentURLProvider())
    try services.register(CurrentUserProvider<Admin>())

    services.register(OffsetPaginatorConfig(perPage: 10, defaultPage: 1))

    var contentConfig = ContentConfig.default()
    
    /// Create custom JSON encoder
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-mm-dd"

    let jsonDecoder = JSONDecoder()
    jsonDecoder.dateDecodingStrategy = .formatted(dateFormatter)

    let jsonEncoder = JSONEncoder()
    jsonEncoder.dateEncodingStrategy = .formatted(dateFormatter)

    /// Register JSON encoder and content config
    contentConfig.use(decoder: jsonDecoder, for: .json)
    contentConfig.use(encoder: jsonEncoder, for: .json)
    services.register(contentConfig)

    services.register { container -> LeafTagConfig in
        var tags = LeafTagConfig.default()
        try tags.useSubmissionsLeafTags(on: container)
        tags.use(InputTag(templatePath: "Submissions/Fields/date-input"), as: "submissions:date")
        tags.use(InputTag(templatePath: "Submissions/Fields/number-input"), as: "submissions:number")
        tags.useFlashLeafTags()
        tags.use(BootstrapProvider.tags)
        tags.use(OffsetPaginatorTag(templatePath: "Paginator/offsetpaginator"), as: "offsetPaginator")
        tags.use(NavItemTag(), as: "ui:navItem")
        tags.use(IsAuthenticatedTag<Admin>(), as: "app:user:isAuthenticated")
        return tags
    }

    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)

    var middlewaresConfig = MiddlewareConfig()
    try middlewares(config: &middlewaresConfig)
    services.register(middlewaresConfig)

    var databasesConfig = DatabasesConfig()
    try databases(config: &databasesConfig)
    services.register(databasesConfig)

    var migrations = MigrationConfig()
    migrations.add(model: Admin.self, database: .psql)
    migrations.add(model: Teacher.self, database: .psql)
    migrations.add(model: Group.self, database: .psql)
    migrations.add(model: Child.self, database: .psql)
    migrations.add(model: ChildGroup.self, database: .psql)
    services.register(migrations)

    config.prefer(LeafRenderer.self, for: ViewRenderer.self)
    config.prefer(MemoryKeyedCache.self, for: KeyedCache.self)
}

public func middlewares(config: inout MiddlewareConfig) throws {
    config.use(FileMiddleware.self)
    config.use(ErrorMiddleware.self)
    config.use(SessionsMiddleware.self)
    config.use(FlashMiddleware.self)
}

public func databases(config: inout DatabasesConfig) throws {
    let postgresqlConfig = PostgreSQLDatabaseConfig(
        hostname: "localhost",
        port: 5432,
        username: "Maciek",
        database: "tin-db"
    )

    let postgresql = PostgreSQLDatabase(config: postgresqlConfig)
    config.add(database: postgresql, as: .psql)
}

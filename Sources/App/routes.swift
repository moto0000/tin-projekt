import Vapor
import Sugar
import Leaf

enum Views {
    static let home = "home"
    static let register = "register"
}

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    let session = Admin.authSessionsMiddleware()
    let currentUser = CurrentUserMiddleware<Admin>()
    let currentURL = CurrentURLMiddleware()
    let auth = router.grouped([session, currentUser, currentURL])

    auth.get("/") { req in
        return try req.leaf().render(Views.home)
    }

    try auth.register(collection: UserController())
    try auth.register(collection: GroupController())
    try auth.register(collection: TeacherController())
    try auth.register(collection: ChildController())
}

extension Request {

    func leaf() throws -> LeafRenderer {
        return try privateContainer.make()
    }
}

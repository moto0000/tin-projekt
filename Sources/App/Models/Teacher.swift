import Vapor
import FluentPostgreSQL
import Submissions
import Validation

final class Teacher: PostgreSQLModel {

    var id: Int?
    
    var name: String
    var lastName: String
    var dateOfBirth: Date
    var phoneNumber: String
    var email: String

    init(name: String, lastName: String, dateOfBirth: Date, phoneNumber: String, email: String) {
        self.name = name
        self.email = email
        self.lastName = lastName
        self.dateOfBirth = dateOfBirth
        self.phoneNumber = phoneNumber
        self.email = email
    }

}

extension Teacher: Migration {

    static func prepare(on conn: PostgreSQLConnection) -> Future<Void> {
        return PostgreSQLDatabase.create(self, on: conn) { builder in
            try addProperties(to: builder, excluding: [
                Teacher.reflectProperty(forKey: \.dateOfBirth),
            ])

            builder.field(for: \.dateOfBirth, type: .date)
            builder.unique(on: \.email)
        }
    }
}

extension Teacher: Content {
    // no-op
}

extension Teacher: Parameter {
    // no-op
}

// MARK: - Submittable

extension Teacher: Submittable {

    struct Submission: SubmissionType {
        var name: String?
        var lastName: String?
        var dateOfBirth: Date?
        var phoneNumber: String?
        var email: String?
    }
    
    struct Create: Decodable {
        var name: String
        var lastName: String
        var dateOfBirth: Date
        var phoneNumber: String
        var email: String
    }

    convenience init(_ create: Create) {
        self.init(
            name: create.name,
            lastName: create.lastName,
            dateOfBirth: create.dateOfBirth,
            phoneNumber: create.phoneNumber,
            email: create.email
        )
    }

    func update(_ submission: Submission) {
        name ?= submission.name
        lastName ?= submission.lastName
        dateOfBirth ?= submission.dateOfBirth
        phoneNumber ?= submission.phoneNumber
        email ?= submission.email
    }
}

extension Teacher.Submission {

    init(_ teacher: Teacher?) {
        name = teacher?.name
        lastName = teacher?.lastName
        dateOfBirth = teacher?.dateOfBirth
        phoneNumber = teacher?.phoneNumber
        email = teacher?.email
    }

    func fieldEntries() throws -> [FieldEntry<Teacher>] {
        return try [
            makeFieldEntry(
                keyPath: \.name,
                label: "Imie",
                validators: [.ascii],
                isRequired: true
            ),
            makeFieldEntry(
                keyPath: \.lastName,
                label: "Nazwisko",
                validators: [.ascii],
                isRequired: true
            ),
            makeFieldEntry(
                keyPath: \.dateOfBirth,
                label: "Data urodzenia",
                // validators: [.ascii],
                isRequired: true
            ),
            makeFieldEntry(
                keyPath: \.phoneNumber,
                label: "Numer telefonu",
                validators: [.phoneNumber(region: "GB")],
                isRequired: true
            ),
            makeFieldEntry(
                keyPath: \.email,
                label: "Adres e-mail",
                validators: [.email],
                isRequired: true
            )
        ]
    }
}

// MARK: -

// import PhoneNumberKit

extension Validator {

    static func phoneNumber(region: String) -> Validator<String> {
        return PhoneNumberValidator(region: region).validator()
    }
}

final class PhoneNumberValidator: ValidatorType {

    var validatorReadable: String {
        // TODO: validatorReadable
        return ""
    }

    private let region: String

    // private let phoneNumberKit = PhoneNumberKit()

    init(region: String) {
        self.region = region
    }

    func validate(_ data: String) throws {
        if data.range(of: "^[0-9]{9}$", options: .regularExpression) == nil {
            throw BasicValidationError("TEST")
        }
        // do {
        //     _ = try phoneNumberKit.parse(data, withRegion: region)
        // } catch {
        //     throw BasicValidationError(error.localizedDescription)
        // }
        
    }
}

infix operator ?= : AssignmentPrecedence

func ?= <T>(lhs: inout T, rhs: T?) {
    guard let value = rhs else {
        return
    }

    lhs = value
}

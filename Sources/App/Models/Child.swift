import Vapor
import Submissions
import FluentPostgreSQL

final class Child: PostgreSQLModel {

    var id: Int?
    
    var name: String
    var lastName: String
    var dateOfBirth: Date
    var phoneNumber: String
    var description: String

    init(name: String, lastName: String, dateOfBirth: Date, phoneNumber: String, description: String) {
        self.name = name
        self.lastName = lastName
        self.dateOfBirth = dateOfBirth
        self.phoneNumber = phoneNumber
        self.description = description
    }
}

extension Child {

    var groups: Siblings<Child, Group, ChildGroup> {
        return siblings()
    }
}

extension Child: Migration {

    static func prepare(on conn: PostgreSQLConnection) -> Future<Void> {
        return PostgreSQLDatabase.create(self, on: conn) { builder in
            try addProperties(to: builder, excluding: [
                Child.reflectProperty(forKey: \.dateOfBirth),
            ])
            
            builder.field(for: \.dateOfBirth, type: .date)
        }
    }
}

extension Child: Content {
    // no-op
}

extension Child: Parameter {
    // no-op
}

// MARK: - Submittable

extension Child: Submittable {
    
    struct Submission: SubmissionType {
        var name: String?
        var lastName: String?
        var dateOfBirth: Date?
        var phoneNumber: String?
        var description: String?
    }
    
    struct Create: Decodable {
        var name: String
        var lastName: String
        var dateOfBirth: Date
        var phoneNumber: String
        var description: String
    }
    
    convenience init(_ create: Create) {
        self.init(
            name: create.name,
            lastName: create.lastName,
            dateOfBirth: create.dateOfBirth,
            phoneNumber: create.phoneNumber,
            description: create.description
        )
    }
    
    func update(_ submission: Submission) {
        name ?= submission.name
        lastName ?= submission.lastName
        dateOfBirth ?= submission.dateOfBirth
        phoneNumber ?= submission.phoneNumber
        description ?= submission.description
    }
}

extension Child.Submission {
    
    init(_ child: Child?) {
        name = child?.name
        lastName = child?.lastName
        dateOfBirth = child?.dateOfBirth
        phoneNumber = child?.phoneNumber
        description = child?.description
    }
    
    func fieldEntries() throws -> [FieldEntry<Child>] {
        return try [
            makeFieldEntry(
                keyPath: \.name,
                label: "Imie",
                validators: [.ascii],
                isRequired: true
            ),
            makeFieldEntry(
                keyPath: \.lastName,
                label: "Nazwisko",
                validators: [.ascii],
                isRequired: true
            ),
            makeFieldEntry(
                keyPath: \.dateOfBirth,
                label: "Data urodzenia",
                // validators: [.ascii],
                isRequired: true
            ),
            makeFieldEntry(
                keyPath: \.phoneNumber,
                label: "Numer telefonu",
                validators: [.phoneNumber(region: "PL")],
                isRequired: true
            ),
            makeFieldEntry(
                keyPath: \.description,
                label: "Uwagi od rodzica",
                validators: [.ascii],
                isRequired: true
            ),
        ]
    }
}

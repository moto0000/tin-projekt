import Vapor
import FluentPostgreSQL

final class ChildGroup: PostgreSQLPivot, ModifiablePivot {
    
    typealias Left = Child
    typealias Right = Group
    
    static var leftIDKey: LeftIDKey = \.childId
    static var rightIDKey: RightIDKey = \.groupId
    
    var id: Int?
    var childId: Child.ID
    var groupId: Group.ID

    init(_ left: Child, _ right: Group) throws {
        childId = try left.requireID()
        groupId = try right.requireID()
    }

    init(childId: Child.ID, groupId: Group.ID) {
        self.childId = childId
        self.groupId = groupId
    }
}

extension ChildGroup: Migration {
    // no-op
}

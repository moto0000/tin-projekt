import Vapor
import FluentPostgreSQL
import Authentication
import Validation
import Submissions

final class Admin: PostgreSQLModel {

    var id: Int?

    var name: String
    var email: String
    var password: String

    init(name: String, email: String, password: String) {
        self.name = name
        self.email = email
        self.password = password
    }
}

extension Admin: Migration {

    static func prepare(on conn: PostgreSQLConnection) -> Future<Void> {
        return PostgreSQLDatabase.create(self, on: conn) { builder in
            try addProperties(to: builder)
            builder.unique(on: \.email)
        }
    }
}

extension Admin: Content {
    // no-op
}

extension Admin: Parameter {
    // no-op
}

// MARK: - Authentication

extension Admin: PasswordAuthenticatable {

    static var usernameKey: UsernameKey = \.email

    static var passwordKey: PasswordKey = \.password
}

extension Admin: SessionAuthenticatable {
    // no-op
}

// MARK: - Login

struct AdminLoginRequest: Content {
    let email: String
    let password: String
}

// MARK: - Submissions

extension Admin: Submittable {

    struct Submission {
        var name: String?
        var email: String?
        var password: String?
    }

    struct Create: Decodable {
        let name: String
        let email: String
        let password: String
    }

    convenience init(_ create: Create) throws {
        try self.init(
            name: create.name,
            email: create.email,
            password: BCrypt.hash(create.password)
        )
    }

    func update(_ submission: Submission) {
        if let name = submission.name {
            self.name = name
        }

        if let email = submission.email {
            self.email = email
        }
    }
}

extension Admin.Submission: SubmissionType {

    init(_ user: Admin?) {
        name = user?.name
        email = user?.email
        password = user?.password
    }

    func fieldEntries() throws -> [FieldEntry<Admin>] {
        return try [
            makeFieldEntry(
                keyPath: \.name,
                label: "Imie i nazwisko",
                validators: [.ascii],
                isRequired: true
            ),
            makeFieldEntry(
                keyPath: \.email,
                label: "E-mail",
                validators: [.email],
                asyncValidators: [
                    unique(keyPath: \.email)
                ],
                isRequired: true
            ),
            makeFieldEntry(
                keyPath: \.password,
                label: "Hasło",
                validators: [.ascii],
                isRequired: true
            )
        ]
    }
}

// MARK: -

import Fluent

protocol AsyncValidatorType {

    func validate(context: ValidationContext, on req: Request) throws -> Future<[ValidationError]>
}

public func unique<S: Model & Submittable, T: Encodable & Equatable>(keyPath: KeyPath<S, T>, message: String = "To pole musi być unikalne") -> Field<S>.Validate<T> {
    return { value, context, entity, req in
        guard context == .create, let value = value else {
            return req.future([])
        }

        var query = S.query(on: req).filter(keyPath == value)

        if let entity = entity {
            query = query.filter(S.idKey != entity[keyPath: S.idKey])
        }

        return query.count().map { count in
            guard count == 0 else {
                return [BasicValidationError(message)]
            }

            return []
        }
    }
}

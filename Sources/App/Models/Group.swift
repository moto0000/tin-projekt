import Vapor
import Submissions
import FluentPostgreSQL

final class Group: PostgreSQLModel {

    var id: Int?

    var name: String
    var description: String
    var maxChildsCount: Int
    var startDate: Date
    var endDate: Date

    var teacherID: Teacher.ID

    init(name: String, description: String, maxChildsCount: Int, startDate: Date, endDate: Date, teacherID: Teacher.ID) {
        self.name = name
        self.description = description
        self.maxChildsCount = maxChildsCount
        self.startDate = startDate
        self.endDate = endDate
        self.teacherID = teacherID
    }
}

extension Group {

    var children: Siblings<Group, Child, ChildGroup> {
        return siblings()
    }

    var teacher: Parent<Group, Teacher> {
        return parent(\.teacherID)
    }
}

extension Group: Migration {
    // no-op
}

extension Group: Content {
    // no-op
}

extension Group: Parameter {
    // no-op
}

// MARK: - Submissions

extension Group: Submittable {

    struct Submission: SubmissionType {
        var name: String?
        var description: String?
        var maxChildsCount: Int?
        var startDate: Date?
        var endDate: Date?
        var teacherID: Teacher.ID?
    }

    func update(_ submission: Submission) {
        name ?= submission.name
        description ?= submission.description
        maxChildsCount ?= submission.maxChildsCount
        startDate ?= submission.startDate
        endDate ?= submission.endDate
        teacherID ?= submission.teacherID
    }
}

extension Group {

    struct Create: Decodable {
        let name: String
        let description: String
        let maxChildsCount: Int
        var startDate: Date
        var endDate: Date
        var teacherID: Teacher.ID
    }

    convenience init(_ create: Create) {
        self.init(
            name: create.name,
            description: create.description,
            maxChildsCount: create.maxChildsCount,
            startDate: create.startDate,
            endDate: create.endDate,
            teacherID: create.teacherID
        )
    }
}

extension Group.Submission {
    
    init(_ group: Group?) {
        name = group?.name
        description = group?.description
        maxChildsCount = group?.maxChildsCount
        startDate = group?.startDate
        endDate = group?.endDate
    }
    
    func fieldEntries() throws -> [FieldEntry<Group>] {
        return try [
            makeFieldEntry(
                keyPath: \.name,
                label: "Nazwa",
                validators: [.ascii],
                isRequired: true
            ),
            makeFieldEntry(
                keyPath: \.description,
                label: "Opis",
                validators: [.ascii],
                isRequired: true
            ),
            makeFieldEntry(
                keyPath: \.maxChildsCount,
                label: "Max. liczba dzieci",
                validators: [.range(1...)],
                isRequired: true
            ),
            makeFieldEntry(
                keyPath: \.startDate,
                label: "Data rozpoczęcia",
                isRequired: true
            ),
            makeFieldEntry(
                keyPath: \.endDate,
                label: "Data zakończenia",
                asyncValidators: [
                    compare(
                        keyPath: \.endDate,
                        isLess: \.startDate,
                        message: "Data zakończenia musi być większa od daty rozpoczęcia"
                    )
                ],
                isRequired: true
            ),
            makeFieldEntry(
                keyPath: \.teacherID,
                label: "Wychowawca",
                asyncValidators: [unique(keyPath: \.teacherID)],
                isRequired: true
            ),
        ]
    }
}

func compare<S: Submittable, T: Comparable>(
    keyPath: KeyPath<S, T>,
    isLess oherKeyPath: KeyPath<S, T>,
    message: String
) -> Field<S>.Validate<T> {
    return { value, context, model, req in
        guard let model = model else {
            return req.future([])
        }

        if model[keyPath: keyPath] > model[keyPath: oherKeyPath] {
            return req.future([])
        }

        return req.future([
            BasicValidationError(message)
        ])
    }
}

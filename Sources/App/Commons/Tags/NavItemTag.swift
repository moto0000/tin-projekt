import Leaf

final class NavItemTag: TagRenderer {

    func render(tag: TagContext) throws -> Future<TemplateData> {
        let body = try tag.requireBody()
        let path = tag.parameters.first?.string ?? "#"
        let currentPath = try tag.container.make(CurrentURLContainer.self).path
        let active: Bool

        if tag.parameters.count > 1 {
            active = isActive(currentPath, patterns: tag.parameters.dropFirst())
        } else {
            active = currentPath == path
        }

        return tag.serializer.serialize(ast: body).map(to: TemplateData.self) { body in
            let parsedBody = String(data: body.data, encoding: .utf8) ?? ""
            
            let item =
            """
            <li class="nav-item">
                <a class="nav-link\(active ? " active" : "")" href="\(path)">
                    \(parsedBody)
                </a>
            </li>
            """

            return .string(item)
        }
    }

    private func isActive(_ path: String, patterns: ArraySlice<TemplateData>) -> Bool {
        for pattern in patterns {
            if let patternString = pattern.string {
                if patternString.hasSuffix("*") {
                    return path.starts(with: patternString.dropLast())
                }

                return path == patternString
            }
        }

        return false
    }
}

import Sugar
import Authentication

final class CurrentUserTag<T: Authenticatable & TemplateDataRepresentable>: TagRenderer {

    func render(tag: TagContext) throws -> Future<TemplateData> {
        try tag.requireParameterCount(1)
        let container = try tag.container.make(CurrentUserContainer<T>.self)

        guard
            let user = container.user,
            let data = try user.convertToTemplateData().dictionary,
            let key = tag.parameters[0].string,
            let value = data[key]
        else {
            throw tag.error(reason: "No user is logged in or the key doesn't exist.")
        }

        return tag.future(value)
    }
}

final class IsAuthenticatedTag<T: Authenticatable>: TagRenderer {

    func render(tag: TagContext) throws -> EventLoopFuture<TemplateData> {
        let container = try tag.container.make(CurrentUserContainer<T>.self)
        return tag.future(.bool(container.user != nil))
    }
}

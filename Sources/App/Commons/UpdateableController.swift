import Leaf
import Vapor
import Fluent
import Submissions

protocol UpdateableController {
    associatedtype Model: Fluent.Model, Parameter, Submittable

    var path: String { get }
    var updateView: String { get }

    func bootUpdate(router: Router) throws

    func update(_ req: Request) throws -> Future<Response>
    func renderUpdate(_ req: Request) throws -> Future<View>
}

extension UpdateableController where Model.ResolvedParameter == Future<Model> {

    var updateView: String {
        return "\(path.capitalized)/edit"
    }

    func bootUpdate(router: Router) throws {
        router.get(path, Model.parameter, "edit", use: renderUpdate)
        router.post(path, Model.parameter, "edit", use: update)
    }

    func update(_ req: Request) throws -> Future<Response> {
        return try req.parameters.next(Model.self)
            .updateValid(on: req)
            .save(on: req)
            .transform(to: req.redirect(to: "/\(path)").flash(.success, "Update OK!"))
            .handleCreateOrUpdateError(on: req, view: updateView)
    }

    func renderUpdate(_ req: Request) throws -> Future<View> {
        return try req.parameters.next(Model.self)
            .populateFields(on: req)
            .flatMap { model in
                try req.leaf().render(self.updateView, ["model": model])
            }
    }
}

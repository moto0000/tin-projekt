import Leaf
import Vapor
import Flash
import Fluent
import Submissions

protocol CreatableController {
    associatedtype Model: Fluent.Model, Submittable

    var path: String { get }
    var createView: String { get }
    var successRedirectPath: String { get }

    func create(_ req: Request) throws -> Future<Response>
    func renderCreate(_ req: Request) throws -> Future<View>

    func bootCreate(router: Router) throws
}

extension CreatableController {

    var createView: String {
        return "\(path.capitalized)/create"
    }

    var successRedirectPath: String {
        return path
    }

    func bootCreate(router: Router) throws {
        router.get(path, "create", use: renderCreate)
        router.post(path, "create", use: create)
    }

    func create(_ req: Request) throws -> Future<Response> {
        return try req.content.decode(Model.Submission.self)
            .createValid(on: req)
            .save(on: req)
            .transform(to: redirectSuccess(on: req))
            .handleCreateOrUpdateError(on: req, view: createView)
    }

    func renderCreate(_ req: Request) throws -> Future<View> {
        try req.populateFields(Model.self)
        return try req.leaf().render(createView)
    }

    private func redirectSuccess(on req: Request) -> Response {
        return req
            .redirect(to: "/\(successRedirectPath)")
            .flash(.success, "OK")
    }
}

extension CreatableController where Self: RouteCollection {

    func boot(router: Router) throws {
        try bootCreate(router: router)
    }
}


extension Future where T == Response {

    func handleCreateOrUpdateError(on req: Request, view: String) throws -> Future<Response> {
        return catchFlatMap { error in
            debugPrint(error)
            return try req.leaf().render(view).flatMap { view in
                try view.encode(for: req)
            }
        }
    }
}

// swift-tools-version:4.0
import PackageDescription

let package = Package(
    name: "TIN",
    dependencies: [
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.0"),
        .package(url: "https://github.com/vapor/leaf.git", from: "3.0.0"),
        .package(url: "https://github.com/vapor/fluent-postgresql.git", from: "1.0.0"),
        .package(url: "https://github.com/vapor/auth.git", from: "2.0.0"),
        .package(url: "https://github.com/nodes-vapor/paginator.git", from: "3.0.0-rc"),
        .package(url: "https://github.com/nodes-vapor/submissions.git", from: "1.0.0-beta"),
        .package(url: "https://github.com/nodes-vapor/bootstrap.git", from: "1.0.0"),
        .package(url: "https://github.com/nodes-vapor/flash.git", .branch("master")),
        .package(url: "https://github.com/marmelroy/PhoneNumberKit.git", from: "2.5.0"),
        .package(url: "https://github.com/stencilproject/Stencil.git", from: "0.13.1"),
    ],
    targets: [
        .target(name: "App", dependencies: [
            "Vapor",
            "Leaf",
            "FluentPostgreSQL",
            "Authentication",
            "Submissions",
            "Paginator",
            "Bootstrap",
            "Flash",
            "PhoneNumberKit",
            "Stencil"
        ]),
        .target(name: "Run", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App"])
    ]
)

